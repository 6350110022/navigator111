import 'package:flutter/material.dart';
import 'package:navigator/src/pages/home.dart';
import 'package:navigator/src/pages/welcome_page.dart';

class App extends StatelessWidget {
  App({Key? key}) : super(key: key);
  final _route = <String, WidgetBuilder>{
    "/page2": (BuildContext context) => WelcomePage(),
  };

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: _route,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const Home(title: 'Basic Navigator'),
    );
  }
}