import 'package:flutter/material.dart';
import 'package:navigator/src/models/login_model.dart';

class Home extends StatefulWidget {
  const Home({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  TextEditingController emailController = new TextEditingController();
  TextEditingController nameController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: TextField(
                controller: emailController,
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.blue,
                ),
                decoration: InputDecoration(
                  hintText: "Please Enter Your Email ",
                  prefixIcon: Icon(
                    Icons.email,
                    color: Colors.blue,
                  ),
                  hintStyle: TextStyle(
                      fontSize: 20,
                      color: Colors.blue,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: TextField(
                controller: nameController,
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.blue,
                ),
                decoration: InputDecoration(
                  hintText: "Please Enter Your Name",
                  prefixIcon: Icon(
                    Icons.people,
                    color: Colors.blue,
                  ),
                  hintStyle: TextStyle(
                    fontSize: 20,
                    color: Colors.blue,
                  ),
                ),
              ),
            ),
            ElevatedButton(
              onPressed: () async {
                var result = await Navigator.pushNamed(
                    context,
                    '/page2',
                  arguments: LoginModel(emailController.text, nameController.text)
                );
                ScaffoldMessenger.of(context)
                ..removeCurrentSnackBar()
                ..showSnackBar(SnackBar(content: Text("$result")));
              },
                child:Text("Next Page"),
            ),
          ],
        ),
      ),
    );
  }
}