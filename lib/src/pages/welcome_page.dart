import 'package:flutter/material.dart';
import 'package:navigator/src/models/login_model.dart';

class WelcomePage extends StatelessWidget {
  const WelcomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final LoginModel login = ModalRoute.of(context)!.settings.arguments as LoginModel;
    return Scaffold(
      appBar: AppBar(
        title: Text("Welcome ${login.reportName}"),
      ),
      body: Container(
        alignment: Alignment.center,
        child: Column(
          children: [
            Text(
              login.reportEmail,
              style: TextStyle(fontSize: 25),
            ),
            Text(
                login.reportName,
                style: TextStyle(fontSize: 25),
            ),
            ElevatedButton(
                onPressed: () {
                 Navigator.pop(context,"สวัสดีค่ะ");
            },
                 child: Text('Back')),
          ],
        ),
      ),
    );
  }
}
